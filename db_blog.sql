-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2017 at 08:02 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `admin_id` int(3) NOT NULL,
  `admn_name` varchar(100) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `admn_name`, `email_address`, `password`) VALUES
(1, 'BITM PHP 41', 'admin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blog`
--

CREATE TABLE `tbl_blog` (
  `blog_id` int(5) NOT NULL,
  `blog_title` varchar(100) NOT NULL,
  `author_name` varchar(100) NOT NULL,
  `blog_description` text NOT NULL,
  `publication_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_blog`
--

INSERT INTO `tbl_blog` (`blog_id`, `blog_title`, `author_name`, `blog_description`, `publication_status`) VALUES
(1, 'Heading Title', 'Jalal Uddin', 'Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. ', 0),
(2, 'à¦¬à§ƒà¦·à§à¦Ÿà¦¿à¦° à¦…à¦¨à¦¿à¦¶à§à¦šà§Ÿà¦¤à¦¾à§Ÿ à¦•à§à¦°à¦¾à¦‡à¦¸à§à¦Ÿà¦šà¦¾à¦°à§à¦š à¦Ÿà§‡ï', 'Rohim Khan', 'à¦•à§à¦°à¦¾à¦‡à¦¸à§à¦Ÿà¦šà¦¾à¦°à§à¦šà§‡à¦° à¦¸à§à¦¥à¦¾à¦¨à§€à§Ÿ à¦¸à¦®à§Ÿ à¦¸à¦•à¦¾à¦² à§§à§¦à¦Ÿà¦¾ à§©à§­ à¦®à¦¿à¦¨à¦¿à¦Ÿà§‡ à¦¤à§ƒà¦¤à§€à§Ÿ à¦¦à¦¿à¦¨à§‡à¦° à¦–à§‡à¦²à¦¾ à¦¶à§à¦°à§ à¦¹à¦“à§Ÿà¦¾à¦° à¦•à¦¥à¦¾ à¦›à¦¿à¦², à¦•à¦¿à¦¨à§à¦¤à§ à¦¬à§ƒà¦·à§à¦Ÿà¦¿à¦° à¦¬à¦¾à¦—à§œà¦¾à§Ÿ à¦à¦–à¦¨ à¦…à¦¨à¦¿à¦¶à§à¦šà§Ÿà¦¤à¦¾à§Ÿ à¦¤à§ƒà¦¤à§€à§Ÿ à¦¦à¦¿à¦¨à§‡à¦° à¦–à§‡à¦²à¦¾à¥¤ à¦¬à§ƒà¦·à§à¦Ÿà¦¿ à¦¥à¦¾à¦®à¦²à§‡ à¦†à¦®à§à¦ªà¦¾à§Ÿà¦¾à¦°à¦°à¦¾ à¦–à§‡à¦²à¦¾ à¦¶à§à¦°à§à¦° à¦¸à¦®à§Ÿ à¦œà¦¾à¦¨à¦¾à¦¬à§‡à¦¨à¥¤ à¦†à¦¬à¦¹à¦¾à¦“à§Ÿà¦¾à¦° à¦ªà§‚à¦°à§à¦¬à¦¾à¦­à¦¾à¦¸à§‡ à¦•à§à¦°à¦¾à¦‡à¦¸à§à¦Ÿà¦šà¦¾à¦°à§à¦šà§‡ à¦†à¦œ à¦¸à¦¾à¦°à¦¾ à¦¦à¦¿à¦¨à¦‡ à¦¬à§ƒà¦·à§à¦Ÿà¦¿à¦° à¦¹à¦“à§Ÿà¦¾à¦° à¦•à¦¥à¦¾ à¦¬à¦²à¦¾ à¦¹à§Ÿà§‡à¦›à§‡à¥¤ à¦®à¦¾à¦ à§‡ à¦¬à¦¾à¦‚à¦²à¦¾à¦¦à§‡à¦¶ à¦¦à¦² à¦à¦²à§‡à¦“ à¦¹à§‡à¦¾à¦Ÿà§‡à¦²à§‡à¦‡ à¦¸à¦®à§Ÿ à¦•à¦¾à¦Ÿà¦¾à¦šà§à¦›à§‡ à¦¨à¦¿à¦‰à¦œà¦¿à¦²à§à¦¯à¦¾à¦¨à§à¦¡à¥¤à¦¦à§à¦¬à¦¿à¦¤à§€à§Ÿ à¦¦à¦¿à¦¨à§‡à¦° à¦–à§‡à¦²à¦¾ à¦¬à¦¨à§à¦§ à¦¹à§Ÿà§‡ à¦¯à¦¾à¦“à§Ÿà¦¾à¦° à¦¸à¦®à§Ÿ à¦¸à¦¾à¦•à¦¿à¦¬ à¦†à¦² à¦¹à¦¾à¦¸à¦¾à¦¨à§‡à¦° à¦¦à§à¦°à§à¦¦à¦¾à¦¨à§à¦¤ à¦¬à§‡à¦¾à¦²à¦¿à¦‚à§Ÿà§‡ à¦®à§à¦¯à¦¾à¦šà§‡ à¦«à¦¿à¦°à§‡ à¦à¦¸à§‡à¦›à¦¿à¦² à¦¬à¦¾à¦‚à¦²à¦¾à¦¦à§‡à¦¶à¥¤ à¦¸à¦¾à¦•à¦¿à¦¬ à¦®à¦¾à¦¤à§à¦° à§¯ à¦¬à¦²à§‡à¦° à¦®à¦§à§à¦¯à§‡ à¦®à¦¿à¦šà§‡à¦² à¦¸à§à¦¯à¦¾à¦¨à§à¦Ÿà¦¨à¦¾à¦°, à¦¬à¦¿à¦œà§‡ à¦“à§Ÿà¦¾à¦Ÿà¦²à¦¿à¦‚ à¦“ à¦•à¦²à¦¿à¦¨ à¦¡à¦¿ à¦—à§à¦°à§à¦¯à¦¾à¦¨à§à¦¡à¦¹à§‡à¦¾à¦®à¦•à§‡ à¦«à¦¿à¦°à¦¿à§Ÿà§‡ à¦¦à¦¿à§Ÿà§‡à¦›à¦¿à¦²à§‡à¦¨à¥¤ à¦ªà§à¦°à¦¥à¦® à¦‡à¦¨à¦¿à¦‚à¦¸à§‡ à¦¬à¦¾à¦‚à¦²à¦¾à¦¦à§‡à¦¶à§‡à¦° à§¨à§®à§¯ à¦°à¦¾à¦¨à§‡à¦° à¦œà¦¬à¦¾à¦¬à§‡ à¦¨à¦¿à¦‰à¦œà¦¿à¦²à§à¦¯à¦¾à¦¨à§à¦¡à§‡à¦° à¦¸à¦‚à¦—à§à¦°à¦¹ à¦›à¦¿à¦² à§­ à¦‰à¦‡à¦•à§‡à¦Ÿà§‡ à§¨à§¬à§¦à¥¤', 0),
(3, 'à¦¬à§‹à¦²à¦¿à¦‚ à¦‰à§Žà¦¸à¦¬à§‡ à¦¶à§‡à¦· à¦¹à¦¾à¦¸à¦¿ à¦¸à¦¾à¦•à¦¿à¦¬à§‡à¦°', 'à¦¤à¦¾à¦°à§‡à¦• à¦®à¦¾à¦¹à¦®à§à¦¦', 'à¦­à§‹à¦œà¦¬à¦¾à¦œà¦¿à¦° à¦®à¦¤à§‹ à¦ªà¦¾à¦²à§à¦Ÿà§‡ à¦—à§‡à¦² à¦¸à¦¬à¦•à¦¿à¦›à§à¥¤ à§ª à¦‰à¦‡à¦•à§‡à¦Ÿà§‡ à§¨à§«à§¨ à¦¥à§‡à¦•à§‡ à§­ à¦‰à¦‡à¦•à§‡à¦Ÿà§‡ à§¨à§«à§¬! à¦¬à¦¾à¦‚à¦²à¦¾à¦¦à§‡à¦¶à§‡à¦° à¦¬à§‹à¦²à¦¾à¦°à¦¦à§‡à¦° à¦¬à¦¿à¦ªà¦•à§à¦·à§‡ à¦¨à¦¿à¦‰à¦œà¦¿à¦²à§à¦¯à¦¾à¦¨à§à¦¡à§‡à¦° à¦¸à¦¾à¦°à¦¾ à¦¦à¦¿à¦¨à§‡à¦° à¦¸à¦‚à¦—à§à¦°à¦¾à¦® à¦ªà¦¿à¦  à¦ à§‡à¦•à¦¿à§Ÿà§‡ à¦«à§‡à¦²à¦² à¦¦à§‡à§Ÿà¦¾à¦²à§‡à¥¤ à¦¶à§‡à¦· à¦­à¦¾à¦²à§‹ à¦¯à¦¾à¦° à¦¸à¦¬ à¦­à¦¾à¦²à§‹ à¦¤à¦¾à¦°à¥¤ à¦•à§à¦°à¦¾à¦‡à¦¸à§à¦Ÿà¦šà¦¾à¦°à§à¦š à¦Ÿà§‡à¦¸à§à¦Ÿà§‡ à¦ªà¦°à¦ªà¦° à¦¦à§à¦¬à¦¿à¦¤à§€à§Ÿ à¦¦à¦¿à¦¨à§‡à¦° à¦®à¦¤à§‹ à¦¹à¦¾à¦¸à¦¿à¦®à§à¦–à§‡ à¦¹à§à¦¯à¦¾à¦—à¦²à¦¿ à¦“à¦­à¦¾à¦² à¦›à¦¾à§œà¦² à¦¬à¦¾à¦‚à¦²à¦¾à¦¦à§‡à¦¶à¥¤à¦•à¦¾à¦² à¦¶à§‡à¦·à¦¬à§‡à¦²à¦¾à§Ÿ à¦¸à¦¾à¦•à¦¿à¦¬à§‡à¦° à¦˜à§‚à¦°à§à¦£à¦¿ à¦¬à¦¾à¦‚à¦²à¦¾à¦¦à§‡à¦¶à§‡à¦° à¦¸à¦¾à¦°à¦¾ à¦¦à¦¿à¦¨à§‡à¦° à¦¬à§‹à¦²à¦¿à¦‚ à¦‰à§Žà¦¸à¦¬à§‡à¦°à¦‡ â€˜à¦ªà§à¦°à§‹à¦®à§‹â€™ à¦¹à¦¿à¦¸à§‡à¦¬à§‡ à¦¦à§‡à¦–à¦¤à§‡ à¦ªà¦¾à¦°à§‡à¦¨à¥¤ à¦ªà§à¦°à§‹à¦®à§‹ à¦¹à¦¤à§‡ à¦ªà¦¾à¦°à§‡ à¦°à§à¦¬à§‡à¦² à¦¹à§‹à¦¸à§‡à¦¨à§‡à¦° à¦“à¦‡ à¦¬à¦²à¦Ÿà¦¾à¦“à¥¤ à¦®à¦¾à¦¥à¦¾ à¦¨à¦¾à¦®à¦¾à¦¨à§‹à¦°à¦“ à¦¸à¦®à§Ÿ à¦ªà¦¾à¦¨à¦¨à¦¿ à¦Ÿà¦® à¦²à§à¦¯à¦¾à¦¥à¦¾à¦®à¥¤ à¦°à§à¦¬à§‡à¦²à§‡à¦° à¦¬à¦¾à¦‰à¦¨à§à¦¸à¦¾à¦° à¦†à¦˜à¦¾à¦¤ à¦•à¦°à¦² à¦¹à§‡à¦²à¦®à§‡à¦Ÿà§‡à¥¤ à¦•à¦¿à¦›à§ à¦¸à¦®à§Ÿà§‡à¦° à¦œà¦¨à§à¦¯ à¦¯à§‡à¦¨ à¦¸à¦‚à¦¬à¦¿à§Ž à¦¹à¦¾à¦°à¦¾à¦²à§‡à¦¨ à¦¨à¦¿à¦‰à¦œà¦¿à¦²à§à¦¯à¦¾à¦¨à§à¦¡ à¦“à¦ªà§‡à¦¨à¦¾à¦°à¥¤ à¦à¦•à¦Ÿà§ à¦§à¦¾à¦¤à¦¸à§à¦¥ à¦¹à§Ÿà§‡ à¦†à¦¬à¦¾à¦° à¦¬à§à¦¯à¦¾à¦Ÿà¦¿à¦‚ à¦•à¦°à¦²à§‡à¦“ à¦¬à¦¦à¦²à§‡ à¦¨à¦¿à¦¤à§‡ à¦¹à§Ÿà§‡à¦›à§‡ à¦¨à¦·à§à¦Ÿ à¦¹à§Ÿà§‡ à¦¯à¦¾à¦“à§Ÿà¦¾ à¦¹à§‡à¦²à¦®à§‡à¦Ÿà¦Ÿà¦¿à¥¤à¦²à§à¦¯à¦¾à¦¥à¦¾à¦®à¦•à§‡ à¦®à¦¾à¦ à¦›à¦¾à§œà¦¾ à¦•à¦°à¦¤à§‡ à¦ªà¦¾à¦°à§‡à¦¨à¦¿ à¦°à§à¦¬à§‡à¦²à§‡à¦° à¦¬à¦¾à¦‰à¦¨à§à¦¸à¦¾à¦°à¥¤ à¦¤à¦¬à§ à¦¹à§à¦¯à¦¾à¦—à¦²à¦¿ à¦“à¦­à¦¾à¦²à§‡ à¦Ÿà§‡à¦¸à§à¦Ÿà§‡à¦° à¦¦à§à¦¬à¦¿à¦¤à§€à§Ÿ à¦¦à¦¿à¦¨à§‡ à¦¬à¦¾à¦‚à¦²à¦¾à¦¦à§‡à¦¶à§‡à¦° à¦¬à§‹à¦²à¦¾à¦°à¦¦à§‡à¦° à¦†à¦•à§à¦°à¦®à¦£à¦¾à¦¤à§à¦®à¦• à¦®à¦¾à¦¨à¦¸à¦¿à¦•à¦¤à¦¾à¦° à¦ªà§à¦°à¦¤à§€à¦• à¦¹à§Ÿà§‡ à¦¥à¦¾à¦•à¦¬à§‡ à¦¸à§‡à¦Ÿà¦¿à¦“à¥¤ à¦¬à¦¾à¦‰à¦¨à§à¦¸à¦¾à¦° à¦†à¦° à¦®à§à¦­à¦®à§‡à¦¨à§à¦Ÿà§‡à¦° à¦¸à¦¾à¦®à¦¨à§‡ à¦•à¦¿à¦‰à¦‡ à¦¬à§à¦¯à¦¾à¦Ÿà¦¸à¦®à§à¦¯à¦¾à¦¨à¦¦à§‡à¦° à¦¬à¦¿à¦¬à§à¦°à¦¤ à¦¹à¦¤à§‡ à¦¹à§Ÿà§‡à¦›à§‡ à¦…à¦¨à§à¦•à§à¦·à¦£à¥¤ à¦¦à§à¦°à§à¦¦à¦¾à¦¨à§à¦¤ à¦¸à¦¬ à¦¬à¦¾à¦‰à¦¨à§à¦¸à¦¾à¦° à¦›à§à§œà§‡à¦›à§‡à¦¨ à¦†à¦—à§‡à¦° à¦¦à¦¿à¦¨ à¦¬à§à¦¯à¦¾à¦Ÿà¦¿à¦‚à§Ÿà§‡à¦° à¦¸à¦®à§Ÿ à¦¡à¦¾à¦¨ à¦¹à¦¾à¦¤à§‡à¦° à¦•à¦¨à§à¦‡à§Ÿà§‡ à¦¬à§à¦¯à¦¥à¦¾ à¦ªà¦¾à¦“à§Ÿà¦¾ à¦°à§à¦¬à§‡à¦²à¥¤ à¦¬à¦²à§‡à¦° à¦—à¦¤à¦¿ à¦¤à§‹ à¦•à§Ÿà§‡à¦•à¦¬à¦¾à¦°à¦‡ à§§à§ªà§¦ à¦•à¦¿à¦²à§‡à¦¾à¦®à¦¿à¦Ÿà¦¾à¦° à¦ªà¦¾à¦° à¦•à¦°à§‡à¦›à§‡à¦¨à¥¤ à¦•à¦¾à¦®à¦°à§à¦² à¦‡à¦¸à¦²à¦¾à¦®à§‡à¦° à¦²à¦¾à¦‡à¦¨-à¦²à§‡à¦‚à¦¥ à¦›à¦¿à¦² à¦¨à¦¿à¦–à§à¦à¦¤à¥¤ à¦¬à§à¦¯à¦¾à¦Ÿà¦¸à¦®à§à¦¯à¦¾à¦¨à¦¦à§‡à¦° à¦¶à¦°à§€à¦° à¦¤à¦¾à¦• à¦•à¦°à¦¾ à¦¬à¦¾à¦‰à¦¨à§à¦¸à¦¾à¦° à¦—à§‡à¦›à§‡ à¦¤à¦¾à¦à¦° à¦¹à¦¾à¦¤ à¦¥à§‡à¦•à§‡à¦“à¥¤ à¦à¦®à¦¨à¦•à¦¿ à¦•à§à¦°à¦¾à¦‡à¦¸à§à¦Ÿà¦šà¦¾à¦°à§à¦šà§‡à¦‡ à¦¦à§à¦¬à¦¿à¦¤à§€à§Ÿ à¦Ÿà§‡à¦¸à§à¦Ÿ à¦–à§‡à¦²à¦¤à§‡ à¦¨à¦¾à¦®à¦¾ à¦¤à¦¾à¦¸à¦•à¦¿à¦¨ à¦†à¦¹à¦®à§‡à¦¦à§‡à¦°à¦“ à¦¬à¦²à§‡à¦° à¦“à¦ªà¦° à¦›à¦¿à¦² à¦­à¦¾à¦²à§‹ à¦¨à¦¿à§Ÿà¦¨à§à¦¤à§à¦°à¦£à¥¤', 1),
(4, 'demo blog Inf', 'Robin Khan', 'The PHP development team announces the immediate availability of PHP 5.6.30. This is a security release. Several security bugs were fixed in this release. All PHP 5.6 users are encouraged to upgrade to this version.\r\nThe PHP development team announces the immediate availability of PHP 5.6.30. This is a security release. Several security bugs were fixed in this release. All PHP 5.6 users are encouraged to upgrade to this version.\r\nThe PHP development team announces the immediate availability of PHP 5.6.30. This is a security release. Several security bugs were fixed in this release. All PHP 5.6 users are encouraged to upgrade to this version.', 1),
(5, '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_image`
--

CREATE TABLE `tbl_image` (
  `image_id` int(2) NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_blog`
--
ALTER TABLE `tbl_blog`
  ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `tbl_image`
--
ALTER TABLE `tbl_image`
  ADD PRIMARY KEY (`image_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `admin_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_blog`
--
ALTER TABLE `tbl_blog`
  MODIFY `blog_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_image`
--
ALTER TABLE `tbl_image`
  MODIFY `image_id` int(2) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
